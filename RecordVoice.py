import pyaudio
import wave
import numpy as np
import os

# Demandez à l'utilisateur ce qu'il souhaite dire
label = input("Que souhaitez-vous dire? (O=Oui/N=Non/1/2/3/4) ")
found = str(label).capitalize()
reponse = [str("O"),str("N"),str("1"),str("2"),str("3"),str("4")]

# Demandez à l'utilisateur si il souhaite le faire 10 fois de suite
label1 = input("Voulez vous effectuer 10 enregistrements d'un coup ? (Oui=1) ")
choice= int(label1)

flag=0
for answer in reponse:
    if answer==found:
        flag=1

# Créez les dossiers s'ils n'existent pas
if not os.path.exists("./enregistrements/"):
    os.makedirs("./enregistrements/")
if not os.path.exists("./enregistrements/O"):
    os.makedirs("./enregistrements/O")
if not os.path.exists("./enregistrements/N"):
    os.makedirs("./enregistrements/N")
if not os.path.exists("./enregistrements/1"):
    os.makedirs("./enregistrements/1")
if not os.path.exists("./enregistrements/2"):
    os.makedirs("./enregistrements/2")
if not os.path.exists("./enregistrements/3"):
    os.makedirs("./enregistrements/3")
if not os.path.exists("./enregistrements/4"):
    os.makedirs("./enregistrements/4")
if not os.path.exists("./enregistrements/5"):
    os.makedirs("./enregistrements/5")

def record():
    # Récupérez tous les fichiers pour ajouter le nouveau
    file_path="./enregistrements/"+label
    number=0
    files_list = []
    for root, directories, files in os.walk(file_path):
        for name in files:            
            number = int(name.split(".wav")[0][1])
            number=number+1
    newFilePath=file_path+"/"+label+str(number)+".wav"


    # Définissez les paramètres de l'enregistrement
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    RECORD_SECONDS = 2

    # Créez un objet PyAudio
    p = pyaudio.PyAudio()

    # Ouvrez un flux d'enregistrement
    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    print("Enregistrement en cours...")

    # Initialisez la liste des données de l'enregistrement
    data = []

    # Boucle d'enregistrement
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        # Lisez les données du flux
        recording = stream.read(CHUNK)
        # Ajoutez les données à la liste
        data.append(recording)

    print("Enregistrement terminé")

    # Créez un fichier audio à partir des données de l'enregistrement
    wave_file = wave.open(newFilePath, "wb")
    wave_file.setnchannels(CHANNELS)
    wave_file.setsampwidth(p.get_sample_size(FORMAT))
    wave_file.setframerate(RATE)
    wave_file.writeframes(b''.join(data))
    wave_file.close()

    # Fermez le flux
    stream.stop_stream()
    stream.close()

    # Fermez l'objet PyAudio
    p.terminate()

if flag==1:
    if choice == 1:
        print("début de la séquence")
        n=0
        while n < 10:
            print(n)
            record()
            n+=1
    else :
        record()
